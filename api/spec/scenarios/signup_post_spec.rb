
describe "POST /signup" do
    context "novo usuario" do
        before(:all) do
            payload = {name: "Pitty" , email: "pitty@bol.com.br", password: "pwd123" }
            MongoDB.new.remove_user(payload[:email])

            @result = Signup.new.create(payload)
        end

        it "valida status code" do
            expect(@result.code).to eql 200
        end

        it "valida id do usuario" do
            expect(@result.parsed_response["_id"].length).to eql 24
        end
    end

    context "usuario ja existe" do
        before(:all) do
            # dado que eu tenho um novo usuario
            payload = {name: "João da Silva" , email: "joao@ig.com.br", password: "pwd123" }
            MongoDB.new.remove_user(payload[:email])

            # e o email desse usuario ja foi cadastrado no sistema
            Signup.new.create(payload)

            # quando faço uma requisição para a rota /signup
            @result = Signup.new.create(payload)
        end

        it "deve retornar 409" do
            # então deve retornar 409
            expect(@result.code).to eql 409
        end

        it "deve retornar mensagem" do
            expect(@result.parsed_response["error"]).to eql "Email already exists :("
        end
    end

    examples = [
        {
        title: "nome em branco",
        payload: {name: "" , email: "teste@uol.com.br", password: "pwd123" }, 
        code: 412,
        error: "required name"  
        },
        {
        title: "nome nao informado",
        payload: { email: "teste@uol.com.br", password: "pwd123" }, 
        code: 412,
        error: "required name"  
        },
        {
        title: "email em branco",
        payload: {name: "Teste" , email: "", password: "pwd123" }, 
        code: 412,
        error: "required email"  
        },
        {
        title: "email nao informado",
        payload: {name: "Teste" , password: "pwd123" }, 
        code: 412,
        error: "required email" 
        },
        {
        title: "senha em branco",
        payload:  {name: "Teste" , email: "teste@uol.com.br", password: "" }, 
        code: 412,
        error: "required password"
        },
        {
        title: "senha nao informado",
        payload:  {name: "Teste" , email: "teste@uol.com.br" }, 
        code: 412,
        error: "required password"
        }
    ]

    examples.each do |e|
        context "#{e[:title]}" do
            before(:all) do
                @result = Signup.new.create(e[:payload])
            end

            it "valida status code #{e[:code]}" do
                expect(@result.code).to eql e[:code]
            end

            it "valida id do usuario" do
                expect(@result.parsed_response["error"]).to eql e[:error]
            end
        end
    end    
end