#language: pt

    Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenário: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais "pamela@gmail.com" e "abc123"
        Então sou redirecionado para o Dashboard

    Esquema do Cenário: Tentar Logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
        | email_input      | senha_input | mensagem_output                  |
        | pamela@gmail.com | abc456      | Usuário e/ou senha inválidos.    |
        | pamela@404.com   | abc123      | Usuário e/ou senha inválidos.    |
        | pamela#gmail.com | abc123      | Oops. Informe um email válido!   |
        |                  | abc123      | Oops. Informe um email válido!   |
        | pamela@gmail.com |             | Oops. Informe sua senha secreta! |

